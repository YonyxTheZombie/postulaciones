import json
import traceback
from itertools import chain
from .__init__ import *
from django.contrib.admin.models import LogEntry, ADDITION, CHANGE
from django.contrib.contenttypes.models import ContentType
from openpyxl import Workbook
from openpyxl.styles import Alignment, Border, PatternFill, Font, Side
from openpyxl.drawing.image import Image
from django.core.mail import EmailMessage, EmailMultiAlternatives
from django.template.loader import get_template,render_to_string
from django.utils.html import strip_tags
from django.template import Context
from django.contrib import messages
from django.contrib.auth import login
from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db.models import Avg, Count, Sum, Min, Max, Func
from django.contrib.auth.models import User
from django.forms import inlineformset_factory, modelformset_factory
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.views.generic import (TemplateView,CreateView, DeleteView, DetailView, ListView,
                                  UpdateView, TemplateView, View)
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from rest_framework.views import APIView
from rest_framework.response import Response
from ..decorators import psicologa_required
from CV_APP.models import Rubrica_equipomulti,Rubrica_demo, Años, Cargo, Colegios, Disposicion, Postulacion, User,  Rubrica, Rubrica_psicologa, Rubrica_coordinador
from CV_APP.forms import PostulacionForm, PostulantesRector, RubricaForm, Entrevistado, Psicologa, Aprobado

def index(request):
    postulantes = Rubrica.objects.filter(postulacion__Siguiente=True ,entrevistado=True,postulacion__psicologa=False, postulacion__rechazar=False, postulacion__aprobar_demos=True)
    postulante_em = Rubrica_equipomulti.objects.filter(postulacion__Siguiente=True ,entrevistado=True,postulacion__psicologa=False, postulacion__rechazar=False, postulacion__aprobar_demos=True)
    postulantes_c = Rubrica_coordinador.objects.filter(entrevistado=True,postulacion__psicologa=False)
    return render(request, "psicologa/index.html", {'postulantes':postulantes,'postulantes_c':postulantes_c,'postulante_em':postulante_em})

def vista_rubrica(request, pk):
    rubrica =get_object_or_404(Rubrica, pk=pk)
    puntos = Rubrica.objects.filter(id=rubrica.pk).values('puntos','puntos2','puntos3','puntos4','puntos5','puntos6','puntos7','puntos8','puntos9','puntos10').aggregate(suma=Sum('puntos') + Sum('puntos2') + Sum('puntos3') + Sum('puntos4') + Sum('puntos5') + Sum('puntos6') + Sum('puntos7') + Sum('puntos8') + Sum('puntos9') + Sum('puntos10'))
    
    return render(request, "psicologa/vista_rubrica.html", {'rubrica':rubrica,'puntos':puntos})


def vista_rubricaC(request, pk):
    rubrica =get_object_or_404(Rubrica_coordinador, pk=pk)
    puntos = Rubrica_coordinador.objects.filter(id=rubrica.pk).values('puntos','puntos2','puntos3','puntos4','puntos5','puntos6','puntos7').aggregate(suma=Sum('puntos') + Sum('puntos2') + Sum('puntos3') + Sum('puntos4') + Sum('puntos5') + Sum('puntos6') + Sum('puntos7'))
    
    return render(request, "psicologa/vista_rubrica_coordinador.html", {'rubrica':rubrica,'puntos':puntos})

def rubrica(request, pk):
    postulantes = get_object_or_404(Postulacion, pk=pk)
    rubrica = Rubrica.objects.filter(postulacion=postulantes)
    opcion = ['Bajo lo esperado','Adecuado', 'Sobre lo esperado']
    if request.method == 'POST':
        form = Aprobado(request.POST, request.FILES, instance=postulantes)
        formset = Psicologa(request.POST, request.FILES)
        opcion = request.POST.getlist('opcion')
        if form.is_valid() and formset.is_valid():
            with transaction.atomic():
                postulante = form.save(commit=False)
                rubrica = formset.save(commit=False)
                rubrica.postulantes = postulantes 
                postulante.save()
                rubrica.save()
            messages.success(request, 'Rubrica realizada')
            return redirect('psicologa:index')
    else:
        form =Aprobado(instance=postulantes)
        formset = Psicologa(instance=postulantes)
    return render(request, "psicologa/rubrica_psicologica.html",{'postulantes':postulantes,'rubrica':rubrica,'form':form,'formset':formset})

def entrevistados(request):
    postulantes = Rubrica.objects.filter(postulacion__psicologa=True)
    rubrica = Rubrica_psicologa.objects.all()
    return render(request, "psicologa/entrevistados.html",{'postulantes':postulantes,'rubrica':rubrica})